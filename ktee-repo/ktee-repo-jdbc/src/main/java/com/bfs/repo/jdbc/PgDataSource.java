package com.bfs.repo.jdbc;

import com.bfs.repo.util.KTeeProperties;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class PgDataSource {

    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static KTeeProperties properties = new KTeeProperties();
    private static DataSource dataSource;

    static {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setSchema(properties.getProperty("db.schema"));
        config.setDriverClassName(properties.getProperty("db.driverName"));
        config.addDataSourceProperty("cachePrepStmts", properties.getProperty("db.cachePrepStmts"));
        config.addDataSourceProperty("prepStmtCacheSize", properties.getProperty("db.prepStmtCacheSize"));
        config.addDataSourceProperty("prepStmtCacheSqlLimit", properties.getProperty("db.prepStmtCacheSqlLimit"));
        logger.info("Hikari datasource configured");
        dataSource = new HikariDataSource(config);
    }

    private PgDataSource() {
    }

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static Connection getConnection() {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            logger.error("database connection not retrieved ", e);
        }

        logger.info("retrieving a connection {}", connection);
        return connection;
    }


    public static void closeConnection(Connection connection){
        if (Objects.nonNull(connection)){
            try {
                connection.close();
                logger.debug("Connection: {} closed " ,connection);
            } catch (SQLException e) {
                logger.error("unable to close connection");
            }
        }
    }

}
