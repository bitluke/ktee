package com.bfs.repo.kteepermission;

import com.bfs.core.KTeePermission;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KTeePermissionRepo {

    public List<KTeePermission> list(Connection connection);

    public void create(KTeePermission kTeePermission, Connection connection);

    public void update(KTeePermission kTeePermission, Connection connection);


    public default KTeePermission find(String id, Connection connection) {
        return find(UUID.fromString(id), connection);
    }

    public KTeePermission find(UUID id, Connection connection);


    public void delete(UUID id, Connection connection);

}
