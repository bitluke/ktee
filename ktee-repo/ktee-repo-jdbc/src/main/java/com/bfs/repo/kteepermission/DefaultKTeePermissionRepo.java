package com.bfs.repo.kteepermission;

import com.bfs.core.KTeePermission;
import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeePermissionAudit;
import com.bfs.core.audit.core.AuditType;
import com.bfs.repo.kteepermission.audit.DefaultKTeePermissionAuditRepo;
import com.bfs.repo.kteepermission.audit.KTeePermissionAuditRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKTeePermissionRepo implements KTeePermissionRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KTeePermissionAuditRepo kTeePermissionAuditRepo = new DefaultKTeePermissionAuditRepo();

    @Override
    public List<KTeePermission> list(Connection connection) {
        List<KTeePermission> kTeePermissions = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM KTEE_PERMISSION");
            while (resultSet.next()) {
                KTeePermission permission = buildPermisionFromResultSet(resultSet);
                kTeePermissions.add(permission);
            }

        } catch (SQLException e) {
            logger.error("selection of ktee role failed  ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection {}", e1);
            }
        }
        return kTeePermissions;
    }


    @Override
    public void create(KTeePermission kTeePermission, Connection connection) {
        kTeePermission.setId(UUID.randomUUID());

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO KTEE_PERMISSION (id, name, description) VALUES (?,?,?)")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, kTeePermission.getId().toString());
            preparedStatement.setString(2, kTeePermission.getName());
            preparedStatement.setString(3, kTeePermission.getDescription());
            preparedStatement.execute();

            createkTeePermissionAudit(kTeePermission,
                    new KTeeUser("tommy", "jerry"), AuditType.Create, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("creation of ktee permission failed ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection {}", e1);
            }
        }
    }

    private void createkTeePermissionAudit(KTeePermission permission,
                                           KTeeUser user,
                                           AuditType auditType,
                                           LocalDateTime dateTime,
                                           Connection connection) {

        KTeePermissionAudit kTeePermissionAudit = new KTeePermissionAudit();
        kTeePermissionAudit.setAuditable(permission);
        kTeePermissionAudit.setAuditType(auditType);
        kTeePermissionAudit.setCreatedBy(user);
        kTeePermissionAudit.setCreationTime(dateTime);

        kTeePermissionAuditRepo.create(kTeePermissionAudit, connection);

    }

    @Override
    public void update(KTeePermission kTeePermission, Connection connection) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("UPDATE KTEE_PERMISSION SET name = ? , description = ? WHERE id = ?")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, kTeePermission.getName());
            preparedStatement.setString(2, kTeePermission.getDescription());
            preparedStatement.setString(3, kTeePermission.getId().toString());
            preparedStatement.execute();

            createkTeePermissionAudit(kTeePermission,
                    new KTeeUser("tommyUpdate", "jerryUpdate"), AuditType.Update, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("update of ktee permission failed  ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection {}", e1);
            }
        }
    }


    @Override
    public KTeePermission find(UUID id, Connection connection) {
        KTeePermission kTeePermission = null;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("SELECT *  FROM KTEE_PERMISSION WHERE id = ?")) {

            preparedStatement.setString(1, id.toString());
            boolean executed = preparedStatement.execute();
            if (executed) {
                ResultSet resultSet = preparedStatement.getResultSet();
                resultSet.next();
                kTeePermission = buildPermisionFromResultSet(resultSet);
            }

        } catch (SQLException sqlException) {
            logger.error("finding of a ktee permission failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
        return kTeePermission;
    }

    private KTeePermission buildPermisionFromResultSet(ResultSet resultSet) throws SQLException {
        KTeePermission permission = new KTeePermission();
        permission.setId(UUID.fromString(resultSet.getString("id")));
        permission.setDescription(resultSet.getString("description"));
        permission.setName(resultSet.getString("name"));
        return permission;
    }

    @Override
    public void delete(UUID id, Connection connection) {
        KTeePermission permissionToDelete = find(id, connection);

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("DELETE FROM KTEE_PERMISSION WHERE id = ?")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, id.toString());
            preparedStatement.execute();


            createkTeePermissionAudit(permissionToDelete,
                    new KTeeUser("tommyDelete", "jerryDelete"), AuditType.Delete, LocalDateTime.now(), connection);


            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("deletion of a ktee permission failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection {}", e1);
            }
        }
    }
}
