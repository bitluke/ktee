package com.bfs.repo.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

public class KTeeProperties {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private Properties properties;

    public KTeeProperties() {
        configure();
    }

    private void configure() {
        //you need to pass the main file .
        //You need to get all property files and load them
        Properties defaultProps = new Properties();
        try {
            defaultProps.load(getClass().getResourceAsStream("/kteerepo_prod.properties"));
        } catch (IOException e) {
            logger.error("Properties file not loaded", e);
        }

        Properties secondProperties = new Properties(defaultProps);
        try {
            secondProperties.load(getClass().getResourceAsStream("/kteerepo_test.properties"));
        } catch (IOException e) {
            logger.error("Properties file not loaded", e);
        }

        Properties thirdProperties  = new Properties(secondProperties);
        try {
            thirdProperties.load(getClass().getResourceAsStream("/kteerepo_dev.properties"));
        } catch (IOException e) {
            logger.error("Properties file not loaded", e);
        }
        properties = new Properties(thirdProperties);
    }

    public String getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }
}
