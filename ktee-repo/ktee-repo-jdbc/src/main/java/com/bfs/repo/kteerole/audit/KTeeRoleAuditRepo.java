package com.bfs.repo.kteerole.audit;

import com.bfs.core.audit.KTeeRoleAudit;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KTeeRoleAuditRepo {
    public List<KTeeRoleAudit> list();

    public void create(KTeeRoleAudit kTeeRoleAudit, Connection connection);

    public void update(KTeeRoleAudit kTeeRoleAudit, Connection connection);

    public KTeeRoleAudit find(UUID id, Connection connection);


}
