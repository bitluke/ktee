package com.bfs.repo.kteerole.audit;

import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeeRoleAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKTeeRoleAuditRepo implements KTeeRoleAuditRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public List<KTeeRoleAudit> list() {
        return new ArrayList<>();
    }

    @Override
    public void create(KTeeRoleAudit kTeeRoleAudit, Connection connection) {
        kTeeRoleAudit.setId(UUID.randomUUID());


        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO KTEE_ROLE_AUDIT (id, audited_id, created_by, audit_type, creation_time, name, description) VALUES (?,?,?,?,?,?,?)")) {

            logger.info("connection commit status {}", connection.getAutoCommit());

            KTeeUser createdBy = kTeeRoleAudit.getCreatedBy();
            String nameFormat = String.format("%s.%s", createdBy.getFirstName(), createdBy.getLastName());

            preparedStatement.setString(1, kTeeRoleAudit.getId().toString());
            preparedStatement.setString(2, kTeeRoleAudit.getAuditable().getId().toString());
            preparedStatement.setString(3, nameFormat);
            preparedStatement.setString(4, kTeeRoleAudit.getAuditType().name());
            preparedStatement.setTimestamp(5, new Timestamp(Instant.now().toEpochMilli()));
            preparedStatement.setString(6, kTeeRoleAudit.getAuditable().getName());
            preparedStatement.setString(7, kTeeRoleAudit.getAuditable().getDescription());
            preparedStatement.execute();

        } catch (SQLException e) {
            logger.error("creation of ktee permission audit failed ", e);
        }
    }

    @Override
    public void update(KTeeRoleAudit kTeeRoleAudit, Connection connection) {

    }

    @Override
    public KTeeRoleAudit find(UUID id, Connection connection) {
        return null;
    }
}
