package com.bfs.repo.kteerole;

import com.bfs.core.KTeeRole;
import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeeRoleAudit;
import com.bfs.core.audit.core.AuditType;
import com.bfs.repo.kteerole.audit.DefaultKTeeRoleAuditRepo;
import com.bfs.repo.kteerole.audit.KTeeRoleAuditRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKteeRoleRepo implements KteeRoleRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KTeeRoleAuditRepo kTeeRoleAuditRepo = new DefaultKTeeRoleAuditRepo();

    public List<KTeeRole> list(Connection connection) {
        List<KTeeRole> kTeeRoles = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM KTEE_ROLE");
            connection.setAutoCommit(false);

            while (resultSet.next()) {
                KTeeRole teeRole = buildKTeeRoleFromResultSet(resultSet);
                kTeeRoles.add(teeRole);
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("selection of ktee role failed  ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
        return kTeeRoles;
    }

    private KTeeRole buildKTeeRoleFromResultSet(ResultSet resultSet) throws SQLException {
        KTeeRole teeRole = new KTeeRole();
        teeRole.setId(UUID.fromString(resultSet.getString("id")));
        teeRole.setDescription(resultSet.getString("description"));
        teeRole.setName(resultSet.getString("name"));
        return teeRole;
    }

    @Override
    public void create(KTeeRole role, Connection connection) {
        role.setId(UUID.randomUUID());

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO KTEE_ROLE (id, name, description) VALUES (?,?,?)")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, role.getId().toString());
            preparedStatement.setString(2, role.getName());
            preparedStatement.setString(3, role.getDescription());
            preparedStatement.execute();

            createkTeePermissionAudit(role,
                    new KTeeUser("tommycreate", "jerrycreate"), AuditType.Create, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("creation of ktee role failed ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    @Override
    public void update(KTeeRole role, Connection connection) {

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("UPDATE KTEE_ROLE SET name = ? , description = ? WHERE id = ?")) {
            connection.setAutoCommit(false);

            preparedStatement.setString(1, role.getName());
            preparedStatement.setString(2, role.getDescription());
            preparedStatement.setString(3, role.getId().toString());
            preparedStatement.executeUpdate();

            createkTeePermissionAudit(role,
                    new KTeeUser("tommyupdate", "jerryupdate"), AuditType.Update, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("update of ktee role failed  ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    @Override
    public KTeeRole find(UUID id, Connection connection) {
        KTeeRole role = null;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("SELECT *  FROM KTEE_ROLE WHERE id = ?")) {

            preparedStatement.setString(1, id.toString());
            boolean executed = preparedStatement.execute();
            if (executed) {
                ResultSet resultSet = preparedStatement.getResultSet();
                resultSet.next();
                role = buildKTeeRoleFromResultSet(resultSet);
            }

        } catch (SQLException sqlException) {
            logger.error("finding of a ktee role failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
        return role;
    }

    @Override
    public void delete(UUID id, Connection connection) {
        KTeeRole kTeeRoleTobeDeleted = find(id, connection);

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("DELETE FROM KTEE_ROLE WHERE id = ?")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, id.toString());
            preparedStatement.executeUpdate();

            createkTeePermissionAudit(kTeeRoleTobeDeleted,
                    new KTeeUser("tommyDelete", "jerryDelete"), AuditType.Delete, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("deleting of a ktee role failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    private void createkTeePermissionAudit(KTeeRole kTeeRole,
                                           KTeeUser user,
                                           AuditType auditType,
                                           LocalDateTime dateTime,
                                           Connection connection) {

        KTeeRoleAudit kTeeRoleAudit = new KTeeRoleAudit();
        kTeeRoleAudit.setAuditable(kTeeRole);
        kTeeRoleAudit.setAuditType(auditType);
        kTeeRoleAudit.setCreatedBy(user);
        kTeeRoleAudit.setCreationTime(dateTime);

        kTeeRoleAuditRepo.create(kTeeRoleAudit, connection);

    }

}
