package com.bfs.repo.kteeuser.audit;

import com.bfs.core.audit.KTeeUserAudit;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KTeeUserAuditRepo {
    public List<KTeeUserAudit> list();

    public void create(KTeeUserAudit kTeeUserAudit, Connection connection);

    public void update(KTeeUserAudit kTeeUserAudit, Connection connection);

    public KTeeUserAudit find(UUID id, Connection connection);


}
