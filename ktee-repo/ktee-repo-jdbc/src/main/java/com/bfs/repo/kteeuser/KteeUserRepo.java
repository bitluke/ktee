package com.bfs.repo.kteeuser;

import com.bfs.core.KTeeUser;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KteeUserRepo {
    public List<KTeeUser> list(Connection connection);

    public void create(KTeeUser user, Connection connection);

    public void update(KTeeUser user, Connection connection);

    public KTeeUser find(UUID id, Connection connection);

    public void delete(UUID id, Connection connection);

}
