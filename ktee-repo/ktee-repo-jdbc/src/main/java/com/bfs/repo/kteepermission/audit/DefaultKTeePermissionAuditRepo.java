package com.bfs.repo.kteepermission.audit;

import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeePermissionAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKTeePermissionAuditRepo implements KTeePermissionAuditRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public List<KTeePermissionAudit> list() {
        return new ArrayList<>();
    }


    @Override
    public void create(KTeePermissionAudit kTeePermissionAudit, Connection connection) {
        kTeePermissionAudit.setId(UUID.randomUUID());


        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO KTEE_PERMISSION_AUDIT (id, audited_id,created_by, audit_type, creation_time, name, description) VALUES (?,?,?,?,?,?,?)")) {

            logger.info("connection commit status {}",connection.getAutoCommit());

            preparedStatement.setString(1, kTeePermissionAudit.getId().toString());
            preparedStatement.setString(2, kTeePermissionAudit.getAuditable().getId().toString());
            KTeeUser createdBy = kTeePermissionAudit.getCreatedBy();
            String nameFormat = String.format("%s.%s", createdBy.getFirstName(), createdBy.getLastName());
            preparedStatement.setString(3, nameFormat);
            preparedStatement.setString(4, kTeePermissionAudit.getAuditType().name());
            preparedStatement.setTimestamp(5, new Timestamp(Instant.now().toEpochMilli()));
            preparedStatement.setString(6, kTeePermissionAudit.getAuditable().getName());
            preparedStatement.setString(7, kTeePermissionAudit.getAuditable().getDescription());
            preparedStatement.execute();

        } catch (SQLException e) {
            logger.error("creation of ktee permission audit failed ", e);
        }
    }

    @Override
    public void create(KTeePermissionAudit kTeePermissionAudit) {
      //not implemented
    }

    @Override
    public void update(KTeePermissionAudit kTeePermissionAudit) {

    }

    @Override
    public KTeePermissionAudit find(UUID id) {
        return null;
    }

}
