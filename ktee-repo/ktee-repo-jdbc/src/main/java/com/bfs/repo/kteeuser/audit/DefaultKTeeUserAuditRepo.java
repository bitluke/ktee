package com.bfs.repo.kteeuser.audit;

import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeeUserAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKTeeUserAuditRepo implements KTeeUserAuditRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public List<KTeeUserAudit> list() {
        return new ArrayList<>();
    }

    @Override
    public void create(KTeeUserAudit kTeeUserAudit, Connection connection) {

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(
                             "INSERT INTO " +
                                     "KTEE_USER_AUDIT (id, audited_id, created_by, audit_type, " +
                                     "creation_time,address, age, city, email, " +
                                     "username, firstname, lastname, middlename," +
                                     "password, sex)" +
                                     "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")) {

            logger.info("connection commit status {}", connection.getAutoCommit());

            kTeeUserAudit.setId(UUID.randomUUID());

            KTeeUser createdBy = kTeeUserAudit.getCreatedBy();
            String nameFormat = String.format("%s.%s", createdBy.getFirstName(), createdBy.getLastName());

            preparedStatement.setString(1, kTeeUserAudit.getId().toString());
            preparedStatement.setString(2, kTeeUserAudit.getAuditable().getId().toString());
            preparedStatement.setString(3, nameFormat);
            preparedStatement.setString(4, kTeeUserAudit.getAuditType().name());
            preparedStatement.setTimestamp(5, new Timestamp(Instant.now().toEpochMilli()));
            preparedStatement.setString(6, kTeeUserAudit.getAuditable().getAddress());
            preparedStatement.setLong(7, kTeeUserAudit.getAuditable().getAge());
            preparedStatement.setString(8, kTeeUserAudit.getAuditable().getCity());
            preparedStatement.setString(9, kTeeUserAudit.getAuditable().getEmail());
            preparedStatement.setString(10, kTeeUserAudit.getAuditable().getUsername());
            preparedStatement.setString(11, kTeeUserAudit.getAuditable().getFirstName());
            preparedStatement.setString(12, kTeeUserAudit.getAuditable().getLastName());
            preparedStatement.setString(13, kTeeUserAudit.getAuditable().getMiddleName());
            preparedStatement.setString(14, kTeeUserAudit.getAuditable().getPassword());
            preparedStatement.setString(15, kTeeUserAudit.getAuditable().getSex());
            preparedStatement.execute();

        } catch (SQLException e) {
            logger.error("creation of ktee permission audit failed ", e);
        }
    }

    @Override
    public void update(KTeeUserAudit kTeeRoleAudit, Connection connection) {

    }

    @Override
    public KTeeUserAudit find(UUID id, Connection connection) {
        return null;
    }
}
