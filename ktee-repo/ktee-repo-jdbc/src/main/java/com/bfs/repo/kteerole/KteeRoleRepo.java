package com.bfs.repo.kteerole;

import com.bfs.core.KTeeRole;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KteeRoleRepo {
    public List<KTeeRole> list(Connection connection);

    public void create(KTeeRole role, Connection connection);

    public void update(KTeeRole role, Connection connection);

    public KTeeRole find(UUID id, Connection connection);

    public void delete(UUID id, Connection connection);

}
