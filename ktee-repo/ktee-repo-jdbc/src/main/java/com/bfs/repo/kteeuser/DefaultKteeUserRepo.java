package com.bfs.repo.kteeuser;

import com.bfs.core.KTeeUser;
import com.bfs.core.audit.KTeeUserAudit;
import com.bfs.core.audit.core.AuditType;
import com.bfs.repo.kteeuser.audit.DefaultKTeeUserAuditRepo;
import com.bfs.repo.kteeuser.audit.KTeeUserAuditRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultKteeUserRepo implements KteeUserRepo {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KTeeUserAuditRepo kTeeUserAuditRepo = new DefaultKTeeUserAuditRepo();

    public List<KTeeUser> list(Connection connection) {
        List<KTeeUser> kTeeUsers = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM KTEE_USER");
            connection.setAutoCommit(false);

            while (resultSet.next()) {
                KTeeUser teeUser = buildKTeeUserFromResultSet(resultSet);
                kTeeUsers.add(teeUser);
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("selection of ktee user failed  ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
        return kTeeUsers;
    }

    private KTeeUser buildKTeeUserFromResultSet(ResultSet resultSet) throws SQLException {
        KTeeUser teeUser = new KTeeUser();
        teeUser.setId(UUID.fromString(resultSet.getString("id")));
        teeUser.setFirstName(resultSet.getString("firstname"));
        teeUser.setLastName(resultSet.getString("lastname"));
        teeUser.setMiddleName(resultSet.getString("middlename"));
        teeUser.setEmail(resultSet.getString("email"));
        teeUser.setCity(resultSet.getString("city"));
        teeUser.setAge(resultSet.getLong("age"));
        teeUser.setAddress(resultSet.getString("address"));
        teeUser.setUsername(resultSet.getString("username"));
        teeUser.setSex(resultSet.getString("sex"));
        teeUser.setPassword(resultSet.getString("password"));
        return teeUser;
    }

    @Override
    public void create(KTeeUser user, Connection connection) {


        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(
                             "INSERT INTO " +
                                     "KTEE_USER (id, address, age, city, email, " +
                                     "username, firstname, lastname, middlename," +
                                     "password, sex)" +
                                     "VALUES (?,?,?,?,?,?,?,?,?,?,?)")) {

            connection.setAutoCommit(false);

            user.setId(UUID.randomUUID());

            preparedStatement.setString(1, user.getId().toString());
            preparedStatement.setString(2, user.getAddress());
            preparedStatement.setLong(3, user.getAge());
            preparedStatement.setString(4, user.getCity());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getUsername());
            preparedStatement.setString(7, user.getFirstName());
            preparedStatement.setString(8, user.getLastName());
            preparedStatement.setString(9, user.getMiddleName());
            preparedStatement.setString(10, user.getPassword());
            preparedStatement.setString(11, user.getSex());
            preparedStatement.execute();

            createkTeeUserAudit(user,
                    new KTeeUser("tommycreate", "jerrycreate"), AuditType.Create, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("creation of ktee user failed ", e);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    @Override
    public void update(KTeeUser user, Connection connection) {

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("UPDATE KTEE_USER SET firstname = ? , lastname = ? , middlename = ?  " +
                             ", address = ? , city = ? , email = ? , username = ? , age = ? , password = ? , sex = ? " +
                             "WHERE id = ?")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getMiddleName());
            preparedStatement.setString(4, user.getAddress());
            preparedStatement.setString(5, user.getCity());
            preparedStatement.setString(6, user.getEmail());
            preparedStatement.setString(7, user.getUsername());
            preparedStatement.setLong(8, user.getAge());
            preparedStatement.setString(9, user.getPassword());
            preparedStatement.setString(10, user.getSex());
            preparedStatement.setString(11, user.getId().toString());
            preparedStatement.executeUpdate();

            createkTeeUserAudit(user,
                    new KTeeUser("tommyupdate", "jerryupdate"), AuditType.Update, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("update of ktee user failed  ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    @Override
    public KTeeUser find(UUID id, Connection connection) {
        KTeeUser user = null;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("SELECT *  FROM KTEE_USER WHERE id = ?")) {

            preparedStatement.setString(1, id.toString());
            boolean executed = preparedStatement.execute();
            if (executed) {
                ResultSet resultSet = preparedStatement.getResultSet();
                resultSet.next();
                user = buildKTeeUserFromResultSet(resultSet);
            }

        } catch (SQLException sqlException) {
            logger.error("finding of a ktee user failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
        return user;
    }

    @Override
    public void delete(UUID id, Connection connection) {
        KTeeUser kTeeUserToDelete = find(id, connection);

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement("DELETE FROM KTEE_USER WHERE id = ?")) {

            connection.setAutoCommit(false);

            preparedStatement.setString(1, id.toString());
            preparedStatement.executeUpdate();


            createkTeeUserAudit(kTeeUserToDelete,
                    new KTeeUser("tommyDelete", "jerryDelete"), AuditType.Delete, LocalDateTime.now(), connection);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException sqlException) {
            logger.error("deleting of a ktee user failed ", sqlException);
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                logger.error("Cannot rollback connection");
            }
        }
    }

    private void createkTeeUserAudit(KTeeUser kTeeUser,
                                     KTeeUser user,
                                     AuditType auditType,
                                     LocalDateTime dateTime,
                                     Connection connection) {

        KTeeUserAudit kTeeUserAudit = new KTeeUserAudit();
        kTeeUserAudit.setAuditable(kTeeUser);
        kTeeUserAudit.setAuditType(auditType);
        kTeeUserAudit.setCreatedBy(user);
        kTeeUserAudit.setCreationTime(dateTime);

        kTeeUserAuditRepo.create(kTeeUserAudit, connection);

    }

}
