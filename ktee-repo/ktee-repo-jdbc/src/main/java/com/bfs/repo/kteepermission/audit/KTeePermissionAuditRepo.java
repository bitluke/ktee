package com.bfs.repo.kteepermission.audit;

import com.bfs.core.audit.KTeePermissionAudit;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public interface KTeePermissionAuditRepo {
    public List<KTeePermissionAudit> list();

    public void create(KTeePermissionAudit kTeePermissionAudit);
    public void create(KTeePermissionAudit kTeePermissionAudit, Connection connection);

    public void update(KTeePermissionAudit kTeePermissionAudit);

    public KTeePermissionAudit find(UUID id);


}
