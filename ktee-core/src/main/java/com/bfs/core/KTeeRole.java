package com.bfs.core;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class KTeeRole {

    private UUID id;
    private String name;
    private String description;
    private Set<KTeeUser> ktusers = new HashSet<KTeeUser>();
    private Set<KTeePermission> permissions = new HashSet<KTeePermission>();


    public KTeeRole() {
        this("", "");
    }

    public KTeeRole(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KTeeRole teeRole = (KTeeRole) o;

        if (!id.equals(teeRole.id)) return false;
        if (name != null ? !name.equals(teeRole.name) : teeRole.name != null) return false;
        return description != null ? description.equals(teeRole.description) : teeRole.description == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
