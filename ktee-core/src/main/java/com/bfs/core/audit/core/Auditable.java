package com.bfs.core.audit.core;

import com.bfs.core.KTeeUser;

import java.time.LocalDateTime;
import java.util.UUID;

public interface Auditable<DomainType> {

    public UUID getId();

    public void setId(UUID uuid);

    public KTeeUser getCreatedBy();

    public void setCreatedBy(KTeeUser kTeeUser);

    public LocalDateTime getCreationTime();

    public void setCreationTime(LocalDateTime localDateTime);

    public AuditType getAuditType();

    public void setAuditType(AuditType auditType);

    public DomainType getAuditable();

    public void setAuditable(DomainType domainType);
}
