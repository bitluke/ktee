package com.bfs.core.audit;

import com.bfs.core.KTeeRole;
import com.bfs.core.KTeeUser;
import com.bfs.core.audit.core.AuditType;
import com.bfs.core.audit.core.Auditable;

import java.time.LocalDateTime;
import java.util.UUID;

public class KTeeRoleAudit implements Auditable<KTeeRole> {

    private UUID id;
    private KTeeRole kTeeRole;
    private KTeeUser createdBy;
    private AuditType auditType;
    private LocalDateTime creationTime;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID uuid) {
        this.id = uuid;
    }

    @Override
    public KTeeUser getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(KTeeUser kTeeUser) {
        this.createdBy = kTeeUser;
    }

    @Override
    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    @Override
    public void setCreationTime(LocalDateTime localDateTime) {
        this.creationTime = localDateTime;
    }

    @Override
    public AuditType getAuditType() {
        return auditType;
    }

    @Override
    public void setAuditType(AuditType auditType) {
        this.auditType = auditType;
    }

    @Override
    public KTeeRole getAuditable() {
        return kTeeRole;
    }

    @Override
    public void setAuditable(KTeeRole kTeeRole) {
        this.kTeeRole = kTeeRole;
    }


}
