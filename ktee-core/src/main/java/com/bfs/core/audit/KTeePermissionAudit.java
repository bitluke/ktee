package com.bfs.core.audit;

import com.bfs.core.KTeePermission;
import com.bfs.core.KTeeUser;
import com.bfs.core.audit.core.AuditType;
import com.bfs.core.audit.core.Auditable;

import java.time.LocalDateTime;
import java.util.UUID;

public class KTeePermissionAudit implements Auditable<KTeePermission> {
    private UUID id;
    private KTeePermission kTeePermission;
    private KTeeUser createdBy;
    private AuditType auditType;
    private LocalDateTime creationTime;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID uuid) {
        this.id = uuid;
    }

    @Override
    public KTeeUser getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(KTeeUser kTeeUser) {
        this.createdBy = kTeeUser;
    }

    @Override
    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    @Override
    public void setCreationTime(LocalDateTime localDateTime) {
        this.creationTime = localDateTime;
    }

    @Override
    public AuditType getAuditType() {
        return auditType;
    }

    @Override
    public void setAuditType(AuditType auditType) {
        this.auditType = auditType;
    }

    @Override
    public KTeePermission getAuditable() {
        return kTeePermission;
    }

    @Override
    public void setAuditable(KTeePermission kTeePermission) {
        this.kTeePermission = kTeePermission;
    }


}
