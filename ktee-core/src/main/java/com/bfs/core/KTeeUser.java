package com.bfs.core;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class KTeeUser {

    private UUID id;
    private String firstName;
    private String middleName;
    private String email;
    private String password;
    private String username;
    private String city;
    private Long age;
    private String address;
    private String sex;
    private String lastName;
    private Set<KTeeRole> roles = new HashSet<KTeeRole>();

    public KTeeUser() {
        this("", "");
    }

    public KTeeUser(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<KTeeRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<KTeeRole> roles) {
        this.roles = roles;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
