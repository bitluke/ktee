package com.bfs.main;

import com.bfs.repo.jdbc.DBVersioningManager;
import com.bfs.repo.jdbc.PgDataSource;
import com.bfs.web.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;


public class KTeeApplication {

    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private HttpServer httpServer;

    public KTeeApplication(HttpServer httpServer) {
        this.httpServer = httpServer;
    }


    public static void main(String[] args) {
        logger.info("Ktee Application starting...");
        DBVersioningManager.migrate(PgDataSource.getDataSource());
        HttpServer httpServer = HttpServer.createHttpServer();
        httpServer.startServer();
    }


}
