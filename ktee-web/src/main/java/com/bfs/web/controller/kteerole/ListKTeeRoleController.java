package com.bfs.web.controller.kteerole;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListKTeeRoleController implements Controller {

    private KTeeRoleService kTeeRoleService;

    public ListKTeeRoleController() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        List<KTeeRoleDto> kTeeRoleDtoList = kTeeRoleService.listKTeeRoles();

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("kTeeRoles", kTeeRoleDtoList);

        templateEngine.process("kteerole/list", ctx, response.getWriter());
    }
}
