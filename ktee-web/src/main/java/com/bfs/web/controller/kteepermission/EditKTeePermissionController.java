package com.bfs.web.controller.kteepermission;

import com.bfs.usecase.kteepermission.DefaultKTeePermissionService;
import com.bfs.usecase.kteepermission.KTeePermissionDto;
import com.bfs.usecase.kteepermission.KTeePermissionService;
import com.bfs.web.StringUtil;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class EditKTeePermissionController implements Controller{


    private Map<String, List<String>> errors = new HashMap<>();
    private KTeePermissionService kTeePermissionService;

    public EditKTeePermissionController() {
        this.kTeePermissionService = new DefaultKTeePermissionService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {


        String requestURI = request.getRequestURI();

        KTeePermissionDto kTeePermissionDto = kTeePermissionService.findkTeePermission(
                UUID.fromString(StringUtil.getUUID(requestURI)));

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("permission", kTeePermissionDto);
        ctx.setVariable("errors", errors);

        templateEngine.process("kteepermission/create", ctx, response.getWriter());

    }



}
