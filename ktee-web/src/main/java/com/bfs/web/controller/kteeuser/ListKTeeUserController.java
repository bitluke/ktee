package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListKTeeUserController implements Controller {

    private KTeeUserService kTeeUserService;

    public ListKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        List<KTeeUserDto> kTeeUserDtoList = kTeeUserService.listKTeeUsers();

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("kTeeUsers", kTeeUserDtoList);

        templateEngine.process("kteeuser/list", ctx, response.getWriter());
    }
}
