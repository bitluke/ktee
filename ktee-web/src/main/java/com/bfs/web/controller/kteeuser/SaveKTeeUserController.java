package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.controller.core.Controller;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

import static com.bfs.web.listener.ValidationContextListener.VALIDATOR;

public class SaveKTeeUserController implements Controller {

    private KTeeUserService kTeeUserService;
    private Map<String, List<String>> errors = new HashMap<>();

    public SaveKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {


        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        KTeeUserDto user = buildDtoFromRequest(request);
        validate(user, servletContext);

        if (errors.isEmpty()) {
            if (user.getId() != null) {
                kTeeUserService.updateKteeUser(user);
            } else {
                kTeeUserService.createKteeUser(user);
            }
            response.sendRedirect(request.getContextPath() + "/user/list");
        } else {

            ctx.setVariable("user", user);
            ctx.setVariable("errors", errors);

            templateEngine.process("kteeuser/create", ctx, response.getWriter());
        }

    }


    private KTeeUserDto buildDtoFromRequest(HttpServletRequest request) {
        KTeeUserDto kTeeUserDto = new KTeeUserDto();
        kTeeUserDto.setFirstName(request.getParameter("firstname"));
        kTeeUserDto.setLastName(request.getParameter("lastname"));
        kTeeUserDto.setMiddleName(request.getParameter("middlename"));
        kTeeUserDto.setUsername(request.getParameter("username"));
        kTeeUserDto.setAddress(request.getParameter("address"));
        kTeeUserDto.setCity(request.getParameter("city"));
        kTeeUserDto.setAge(Long.parseLong(request.getParameter("age")));
        kTeeUserDto.setEmail(request.getParameter("email"));
        kTeeUserDto.setPassword(request.getParameter("password"));
        kTeeUserDto.setSex(request.getParameter("sex"));
        String id = request.getParameter("id");
        if (Objects.nonNull(id)) {
            kTeeUserDto.setId(UUID.fromString(id));
        }
        return kTeeUserDto;
    }

    private void validate(KTeeUserDto kTeeUserDto, ServletContext servletContext) {
        Validator validator = (Validator) servletContext.getAttribute(VALIDATOR);
        Set<ConstraintViolation<KTeeUserDto>> constraintViolations = validator.validate(kTeeUserDto);
        errors.clear();
        collectViolationsByProperty(constraintViolations);
    }

    private void collectViolationsByProperty(Set<ConstraintViolation<KTeeUserDto>> kteeViolations) {

        for (ConstraintViolation<KTeeUserDto> kteeViolation : kteeViolations) {
            String propertyPath = ((PathImpl) kteeViolation.getPropertyPath()).asString();
            List<String> propertyPathViolations = errors.get(propertyPath);
            if (propertyPathViolations == null) {
                propertyPathViolations = new ArrayList<>();
            }
            propertyPathViolations.add(kteeViolation.getMessage());
            errors.put(propertyPath, propertyPathViolations);
        }

    }
}
