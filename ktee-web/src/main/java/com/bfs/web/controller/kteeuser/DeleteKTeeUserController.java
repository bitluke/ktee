package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.StringUtil;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DeleteKTeeUserController implements Controller {

    private KTeeUserService kTeeUserService;
    private Map<String, List<String>> errors = new HashMap<>();

    public DeleteKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        String requestURI = request.getRequestURI();

        kTeeUserService.deleteKTeeUsers(UUID.fromString(StringUtil.getUUID(requestURI)));

        response.sendRedirect( request.getContextPath() + "/user/list");

    }


}
