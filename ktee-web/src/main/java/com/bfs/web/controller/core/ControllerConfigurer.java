package com.bfs.web.controller.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;


public class ControllerConfigurer {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private TemplateEngine templateEngine;

    public ControllerConfigurer(final ServletContext servletContext) {
        super();
        ServletContextTemplateResolver templateResolver = Resolver.configure(servletContext);
        templateEngine = KTeeTemplateEngine.configure(templateResolver);
        Router.configure();
    }

    private static String getRequestPath(final HttpServletRequest request) {

        String requestURI = request.getRequestURI();
        final String contextPath = request.getContextPath();

        final int fragmentIndex = requestURI.indexOf(';');
        if (fragmentIndex != -1) {
            requestURI = requestURI.substring(0, fragmentIndex);
        }

        if (requestURI.startsWith(contextPath)) {
            return requestURI.substring(contextPath.length());
        }
        return requestURI;
    }

    public Controller resolveControllerForRequest(final HttpServletRequest request) {
        final String path = getRequestPath(request);
        String httpMethod = request.getMethod();
        String methodAndPathPattern = String.format("%s:%s", httpMethod, path);
        Controller controller = Router.getController(methodAndPathPattern);
        return controller;
    }

    public ITemplateEngine getTemplateEngine() {
        return this.templateEngine;
    }


}
