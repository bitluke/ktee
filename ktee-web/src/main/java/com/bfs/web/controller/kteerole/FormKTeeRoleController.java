package com.bfs.web.controller.kteerole;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormKTeeRoleController implements Controller{

    private KTeeRoleService kTeeRoleService;
    private Map<String, List<String>> errors = new HashMap<>();

    public FormKTeeRoleController() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        ctx.setVariable("role", new KTeeRoleDto());
        ctx.setVariable("errors", errors);

        templateEngine.process("kteerole/create", ctx, response.getWriter());
    }
}
