package com.bfs.web.controller.kteerole;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class ViewKTeeRoleController implements Controller {

    private KTeeRoleService kTeeRoleService;

    public ViewKTeeRoleController() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        String requestURI = request.getRequestURI();

        KTeeRoleDto kTeeRoleDto = kTeeRoleService.findKteeRole(UUID.fromString(requestURI.substring(requestURI.lastIndexOf("/")+1)));

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("kteerole", kTeeRoleDto);

        templateEngine.process("kteerole/view", ctx, response.getWriter());
    }
}
