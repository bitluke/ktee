
package com.bfs.web.controller.core;

import org.thymeleaf.ITemplateEngine;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public interface Controller extends Serializable{


    public void process(HttpServletRequest request,
                        HttpServletResponse response,
                        ServletContext servletContext,
                        ITemplateEngine templateEngine) throws Exception;


}
