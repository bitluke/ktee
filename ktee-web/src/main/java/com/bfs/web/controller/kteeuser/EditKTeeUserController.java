package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.StringUtil;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class EditKTeeUserController implements Controller {

    private KTeeUserService kTeeUserService;
    private Map<String, List<String>> errors = new HashMap<>();

    public EditKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {


        String requestURI = request.getRequestURI();

        KTeeUserDto kTeeUserDto = kTeeUserService.findKteeUser(
                UUID.fromString(StringUtil.getUUID(requestURI)));

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("user", kTeeUserDto);
        ctx.setVariable("errors", errors);

        templateEngine.process("kteeuser/create", ctx, response.getWriter());

    }



}
