package com.bfs.web.controller;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Error500Controller implements Controller{

    private KTeeRoleService kTeeRoleService;

    public Error500Controller() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        ctx.setVariable("role", new KTeeRoleDto());

        templateEngine.process("error/500", ctx, response.getWriter());
    }
}
