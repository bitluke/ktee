package com.bfs.web.controller;

import com.bfs.web.controller.core.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;
import java.util.Calendar;


public class HomeController implements Controller {

    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void process(HttpServletRequest request,
                        HttpServletResponse response,
                        ServletContext servletContext,
                        ITemplateEngine templateEngine) throws Exception {
        logger.info("Home controller called");

        WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("today", Calendar.getInstance());
        templateEngine.process("home", ctx, response.getWriter());

        logger.info("Home controller finished processing");
    }
}
