package com.bfs.web.controller.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import java.lang.invoke.MethodHandles;

public class Resolver {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static ServletContextTemplateResolver templateResolver;

    private Resolver() {
    }

    public static ServletContextTemplateResolver configure(ServletContext servletContext) {
        templateResolver = new ServletContextTemplateResolver(servletContext);
        templateResolver.setTemplateMode(TemplateMode.HTML);
        // This will convert "home" to "/webapp/home.html"
        templateResolver.setPrefix("/");
        templateResolver.setSuffix(".html");
        // Set template cache TTL to 1 hour.
        // If not set, entries would live in cache until expelled by LRU
        templateResolver.setCacheTTLMs(0L);//setting for dev purposes
        // Cache is set to true by default. Set to false if you want templates to
        // be automatically updated when modified.
        templateResolver.setCacheable(false); //setting for dev purposes
        logger.info("Resolver configured");
        return templateResolver;
    }
}
