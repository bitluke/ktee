package com.bfs.web.controller.kteerole;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.StringUtil;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DeleteKTeeRoleController implements Controller {

    private KTeeRoleService kTeeRoleService;
    private Map<String, List<String>> errors = new HashMap<>();

    public DeleteKTeeRoleController() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        String requestURI = request.getRequestURI();

        kTeeRoleService.deleteKTeeRoles(UUID.fromString(StringUtil.getUUID(requestURI)));

        response.sendRedirect( request.getContextPath() + "/role/list");

    }


}
