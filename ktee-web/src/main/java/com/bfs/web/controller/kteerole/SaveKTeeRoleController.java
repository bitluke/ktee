package com.bfs.web.controller.kteerole;

import com.bfs.usecase.kteerole.DefaultKTeeRoleService;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import com.bfs.usecase.kteerole.KTeeRoleService;
import com.bfs.web.controller.core.Controller;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

import static com.bfs.web.listener.ValidationContextListener.VALIDATOR;

public class SaveKTeeRoleController implements Controller {

    private KTeeRoleService kTeeRoleService;
    private Map<String, List<String>> errors = new HashMap<>();

    public SaveKTeeRoleController() {
        this.kTeeRoleService = new DefaultKTeeRoleService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {


        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        KTeeRoleDto role = buildDtoFromRequest(request);
        validate(role, servletContext);

        if (errors.isEmpty()) {
            if (role.getId() != null) {
                kTeeRoleService.updateKteeRole(role);
            } else {
                kTeeRoleService.createKteeRole(role);
            }
            response.sendRedirect(request.getContextPath() + "/role/list");
        } else {

            ctx.setVariable("role", role);
            ctx.setVariable("errors", errors);

            templateEngine.process("kteerole/create", ctx, response.getWriter());
        }

    }


    private KTeeRoleDto buildDtoFromRequest(HttpServletRequest request) {
        KTeeRoleDto kTeeRoleDto = new KTeeRoleDto();
        kTeeRoleDto.setName(request.getParameter("name"));
        kTeeRoleDto.setDescription(request.getParameter("description"));
        String id = request.getParameter("id");
        if (Objects.nonNull(id)) {
            kTeeRoleDto.setId(UUID.fromString(id));
        }
        return kTeeRoleDto;
    }

    private void validate(KTeeRoleDto kTeeRoleDto, ServletContext servletContext) {
        Validator validator = (Validator) servletContext.getAttribute(VALIDATOR);
        Set<ConstraintViolation<KTeeRoleDto>> constraintViolations = validator.validate(kTeeRoleDto);
        errors.clear();
        collectViolationsByProperty(constraintViolations);
    }

    private void collectViolationsByProperty(Set<ConstraintViolation<KTeeRoleDto>> kteeViolations) {

        for (ConstraintViolation<KTeeRoleDto> kteeViolation : kteeViolations) {
            String propertyPath = ((PathImpl) kteeViolation.getPropertyPath()).asString();
            List<String> propertyPathViolations = errors.get(propertyPath);
            if (propertyPathViolations == null) {
                propertyPathViolations = new ArrayList<>();
            }
            propertyPathViolations.add(kteeViolation.getMessage());
            errors.put(propertyPath, propertyPathViolations);
        }

    }
}
