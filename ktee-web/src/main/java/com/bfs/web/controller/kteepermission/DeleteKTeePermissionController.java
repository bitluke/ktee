package com.bfs.web.controller.kteepermission;

import com.bfs.usecase.kteepermission.DefaultKTeePermissionService;
import com.bfs.usecase.kteepermission.KTeePermissionService;
import com.bfs.web.StringUtil;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DeleteKTeePermissionController implements Controller {

    private Map<String, List<String>> errors = new HashMap<>();

    private KTeePermissionService kTeePermissionService;

    public DeleteKTeePermissionController() {
        this.kTeePermissionService = new DefaultKTeePermissionService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        String requestURI = request.getRequestURI();

        kTeePermissionService.deletekTeePermission(UUID.fromString(StringUtil.getUUID(requestURI)));

        response.sendRedirect(request.getContextPath() + "/permission/list");

    }


}
