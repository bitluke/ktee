package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class ViewKTeeUserController implements Controller {

    private KTeeUserService kTeeUserService;

    public ViewKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        String requestURI = request.getRequestURI();

        KTeeUserDto kTeeUserDto = kTeeUserService.findKteeUser(UUID.fromString(requestURI.substring(requestURI.lastIndexOf("/") + 1)));

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("kteeuser", kTeeUserDto);

        templateEngine.process("kteeuser/view", ctx, response.getWriter());
    }
}
