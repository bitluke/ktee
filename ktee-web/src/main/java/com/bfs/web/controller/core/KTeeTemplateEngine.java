package com.bfs.web.controller.core;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import java.lang.invoke.MethodHandles;

public class KTeeTemplateEngine {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static TemplateEngine templateEngine;

    private KTeeTemplateEngine() {
    }

    public static TemplateEngine configure(ServletContextTemplateResolver templateResolver) {
        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.addDialect(new LayoutDialect());
        logger.info("Template engine configured");
        return templateEngine;
    }

}
