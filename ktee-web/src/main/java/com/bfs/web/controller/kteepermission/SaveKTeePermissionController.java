package com.bfs.web.controller.kteepermission;

import com.bfs.usecase.kteepermission.DefaultKTeePermissionService;
import com.bfs.usecase.kteepermission.KTeePermissionDto;
import com.bfs.usecase.kteepermission.KTeePermissionService;
import com.bfs.web.controller.core.Controller;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

import static com.bfs.web.listener.ValidationContextListener.VALIDATOR;

public class SaveKTeePermissionController implements Controller {

    private KTeePermissionService kTeePermissionService;
    private Map<String, List<String>> errors = new HashMap<>();

    public SaveKTeePermissionController() {
        this.kTeePermissionService = new DefaultKTeePermissionService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {


        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        KTeePermissionDto permissionDto = buildDtoFromRequest(request);
        validate(permissionDto, servletContext);

        if (errors.isEmpty()) {
            if (permissionDto.getId() != null) {
                kTeePermissionService.updatekTeePermission(permissionDto);
            } else {
                kTeePermissionService.createkTeePermission(permissionDto);
            }
            response.sendRedirect(request.getContextPath() + "/permission/list");
        } else {

            ctx.setVariable("permission", permissionDto);
            ctx.setVariable("errors", errors);

            templateEngine.process("kteepermission/create", ctx, response.getWriter());
        }

    }


    private KTeePermissionDto buildDtoFromRequest(HttpServletRequest request) {
        KTeePermissionDto kTeePermissionDto = new KTeePermissionDto();
        kTeePermissionDto.setName(request.getParameter("name"));
        kTeePermissionDto.setDescription(request.getParameter("description"));
        String id = request.getParameter("id");
        if (Objects.nonNull(id)) {
            kTeePermissionDto.setId(UUID.fromString(id));
        }
        return kTeePermissionDto;
    }

    private void validate(KTeePermissionDto kTeePermissionDto, ServletContext servletContext) {
        Validator validator = (Validator) servletContext.getAttribute(VALIDATOR);
        Set<ConstraintViolation<KTeePermissionDto>> constraintViolations = validator.validate(kTeePermissionDto);
        errors.clear();
        collectViolationsByProperty(constraintViolations);
    }

    private void collectViolationsByProperty(Set<ConstraintViolation<KTeePermissionDto>> kteeViolations) {

        for (ConstraintViolation<KTeePermissionDto> kteeViolation : kteeViolations) {
            String propertyPath = ((PathImpl) kteeViolation.getPropertyPath()).asString();
            List<String> propertyPathViolations = errors.get(propertyPath);
            if (propertyPathViolations == null) {
                propertyPathViolations = new ArrayList<>();
            }
            propertyPathViolations.add(kteeViolation.getMessage());
            errors.put(propertyPath, propertyPathViolations);
        }

    }
}
