package com.bfs.web.controller.kteepermission;

import com.bfs.usecase.kteepermission.DefaultKTeePermissionService;
import com.bfs.usecase.kteepermission.KTeePermissionDto;
import com.bfs.usecase.kteepermission.KTeePermissionService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormKTeePermissionController implements Controller{

    private KTeePermissionService kTeePermissionService;
    private Map<String, List<String>> errors = new HashMap<>();

    public FormKTeePermissionController() {
        this.kTeePermissionService = new DefaultKTeePermissionService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        ctx.setVariable("permission", new KTeePermissionDto());
        ctx.setVariable("errors", errors);

        templateEngine.process("kteepermission/create", ctx, response.getWriter());
    }
}
