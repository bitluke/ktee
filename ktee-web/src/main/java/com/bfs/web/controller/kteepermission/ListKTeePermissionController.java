package com.bfs.web.controller.kteepermission;

import com.bfs.usecase.kteepermission.DefaultKTeePermissionService;
import com.bfs.usecase.kteepermission.KTeePermissionDto;
import com.bfs.usecase.kteepermission.KTeePermissionService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListKTeePermissionController implements Controller {

    private KTeePermissionService kTeePermissionService;

    public ListKTeePermissionController() {
        this.kTeePermissionService = new DefaultKTeePermissionService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        List<KTeePermissionDto> kTeePermissionDtos = kTeePermissionService.listkTeePermission();

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
        ctx.setVariable("kTeePermissions", kTeePermissionDtos);

        templateEngine.process("kteepermission/list", ctx, response.getWriter());
    }
}
