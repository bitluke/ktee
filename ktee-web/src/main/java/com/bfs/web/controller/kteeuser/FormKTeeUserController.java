package com.bfs.web.controller.kteeuser;

import com.bfs.usecase.kteeuser.DefaultKTeeUserService;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import com.bfs.usecase.kteeuser.KTeeUserService;
import com.bfs.web.controller.core.Controller;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormKTeeUserController implements Controller{

    private KTeeUserService kTeeUserService;
    private Map<String, List<String>> errors = new HashMap<>();

    public FormKTeeUserController() {
        this.kTeeUserService = new DefaultKTeeUserService();
    }

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response,
                        ServletContext servletContext, ITemplateEngine templateEngine) throws Exception {

        final WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());

        ctx.setVariable("user", new KTeeUserDto());
        ctx.setVariable("errors", errors);

        templateEngine.process("kteeuser/create", ctx, response.getWriter());
    }
}
