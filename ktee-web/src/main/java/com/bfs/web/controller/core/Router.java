package com.bfs.web.controller.core;

import com.bfs.web.controller.Error500Controller;
import com.bfs.web.controller.HomeController;
import com.bfs.web.controller.kteepermission.*;
import com.bfs.web.controller.kteerole.*;
import com.bfs.web.controller.kteeuser.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Router {
    public static final String UUIDPattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}";
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static Map<String, Controller> urlPathToControllerMap = new HashMap<>();;

    private Router() {
    }

    public static void configure() {
        configureHomeRoutes();
        configureRoleRoutes();
        configurePermissionRoutes();
        configureUserRoutes();
        logger.info("Router configured");
    }

    private static void configureHomeRoutes() {
        urlPathToControllerMap.put("/", new HomeController());
        logger.debug("/ mapped to {} ", HomeController.class);
    }

    private static void configureRoleRoutes() {
        urlPathToControllerMap.put("GET:/role/list", new ListKTeeRoleController());
        logger.debug("GET method of /role/list mapped to {}", ListKTeeRoleController.class);

        urlPathToControllerMap.put("POST:/role/create", new SaveKTeeRoleController());
        logger.debug("POST method of /role/create mapped to {}", SaveKTeeRoleController.class);

        urlPathToControllerMap.put("GET:/role/create", new FormKTeeRoleController());
        logger.debug("GET method of /role/create mapped to {}", FormKTeeRoleController.class);

        urlPathToControllerMap.put("GET:/role/view/" + UUIDPattern, new ViewKTeeRoleController());
        logger.debug("GET method of /role/view/id mapped to {}", ViewKTeeRoleController.class);

        urlPathToControllerMap.put("GET:/role/" + UUIDPattern + "/edit", new EditKTeeRoleController());
        logger.debug("GET method of /role/edit mapped to {}", EditKTeeRoleController.class);

        urlPathToControllerMap.put("GET:/role/" + UUIDPattern + "/delete", new DeleteKTeeRoleController());
        logger.debug("GET method of /role/id/delete mapped to {}", DeleteKTeeRoleController.class);

    }


    private static void configureUserRoutes() {
        urlPathToControllerMap.put("GET:/user/list", new ListKTeeUserController());
        logger.debug("GET method of /user/list mapped to {}", ListKTeeUserController.class);

        urlPathToControllerMap.put("POST:/user/create", new SaveKTeeUserController());
        logger.debug("POST method of /user/create mapped to {}", SaveKTeeUserController.class);

        urlPathToControllerMap.put("GET:/user/create", new FormKTeeUserController());
        logger.debug("GET method of /user/create mapped to {}", FormKTeeUserController.class);

        urlPathToControllerMap.put("GET:/user/view/" + UUIDPattern, new ViewKTeeUserController());
        logger.debug("GET method of /user/view/id mapped to {}", ViewKTeeUserController.class);

        urlPathToControllerMap.put("GET:/user/" + UUIDPattern + "/edit", new EditKTeeUserController());
        logger.debug("GET method of /user/edit mapped to {}", EditKTeeUserController.class);

        urlPathToControllerMap.put("GET:/user/" + UUIDPattern + "/delete", new DeleteKTeeUserController());
        logger.debug("GET method of /user/id/delete mapped to {}", DeleteKTeeUserController.class);

    }

    private static void configurePermissionRoutes() {
        urlPathToControllerMap.put("GET:/permission/list", new ListKTeePermissionController());
        logger.debug("GET method of /permission/list mapped to {}", ListKTeePermissionController.class);

        urlPathToControllerMap.put("POST:/permission/create", new SaveKTeePermissionController());
        logger.debug("POST method of /permission/create mapped to {}", SaveKTeePermissionController.class);

        urlPathToControllerMap.put("GET:/permission/create", new FormKTeePermissionController());
        logger.debug("GET method of /permission/create mapped to {}", FormKTeePermissionController.class);

        urlPathToControllerMap.put("GET:/permission/view/" + UUIDPattern, new ViewKTeePermissionController());
        logger.debug("GET method of /permission/view/id mapped to {}", ViewKTeePermissionController.class);

        urlPathToControllerMap.put("GET:/permission/" + UUIDPattern + "/edit", new EditKTeePermissionController());
        logger.debug("GET method of /permission/edit mapped to {}", EditKTeePermissionController.class);

        urlPathToControllerMap.put("GET:/permission/" + UUIDPattern + "/delete", new DeleteKTeePermissionController());
        logger.debug("GET method of /permission/id/delete mapped to {}", DeleteKTeePermissionController.class);

    }

    private static void error500() {
        urlPathToControllerMap.put("GET:/error/500", new Error500Controller());
        logger.info("GET method of /error/500 mapped to {}", Error500Controller.class);
    }


    public static Controller getController(String path) {

        Optional<Map.Entry<String, Controller>> firstEntry = urlPathToControllerMap.entrySet().stream()
                .filter(entry -> path.matches(entry.getKey()))
                .findFirst();

        if (firstEntry.isPresent()) {
            return firstEntry.get().getValue();
        } else {
            return null;
        }
    }

}
