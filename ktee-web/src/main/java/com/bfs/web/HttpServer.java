package com.bfs.web;

import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.resource.PathResource;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.lang.invoke.MethodHandles;
import java.lang.management.ManagementFactory;


public class HttpServer implements Serializable {

    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private HttpServer() {
    }

    public static HttpServer createHttpServer() {
        return new HttpServer();
    }


    public void startServer() {
        try {
            Server server = createServer();
            server.start();
            logger.info("KTee Server started..");
            server.join();
        } catch (InterruptedException iexception) {
            logger.error("Server interrupted", iexception);
        } catch (Exception exception) {
            logger.error("Server not started", exception);
        }

    }

    private Server createServer() {
        Server server = new Server(8080);
        try {
            MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
            server.addBean(mbContainer);
            //server.dumpStdErr();
            server.setStopAtShutdown(true);
            WebAppContext webapp = new WebAppContext();
            webapp.setContextPath("/ktee");
            webapp.setWar(PathResource.newClassPathResource("webapp/").getFile().getAbsolutePath());
            server.setHandler(webapp);
        } catch (IOException ioe) {
            logger.error("Web serving folder not present ", ioe);
        }
        return server;
    }


}
