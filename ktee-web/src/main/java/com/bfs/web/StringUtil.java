package com.bfs.web;

import com.bfs.web.controller.core.Router;

import java.util.stream.Stream;

public class StringUtil {


    public static String getUUID(String string) {
        return Stream.of(string.split("/"))
                .filter(str -> str.matches(Router.UUIDPattern))
                .findFirst().get();

    }
}
