
package com.bfs.web.filter;

import com.bfs.web.controller.core.Controller;
import com.bfs.web.controller.core.ControllerConfigurer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.invoke.MethodHandles;


public class ThymeleafFilter implements Filter {

    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ServletContext servletContext;
    private ControllerConfigurer controllerConfigurer;


    public void init(final FilterConfig filterConfig) throws ServletException {
        this.servletContext = filterConfig.getServletContext();
        this.controllerConfigurer = new ControllerConfigurer(this.servletContext);
    }


    public void doFilter(final ServletRequest request, final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {

        if (!process((HttpServletRequest) request, (HttpServletResponse) response)) {
            chain.doFilter(request, response);
        }
    }


    public void destroy() {
        // nothing to do
    }


    private boolean process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        try {

            // This prevents triggering engine executions for resource URLs
            String contextPath = request.getContextPath();
            if (request.getRequestURI().startsWith(contextPath + "/static/css") ||
                    request.getRequestURI().startsWith(contextPath +"/static/img") ||
                    request.getRequestURI().startsWith(contextPath +"/static/fonts") ||
                    request.getRequestURI().startsWith(contextPath +"/static/js")) {
                return false;
            }

            
            /*
             * Query controller/URL mapping and obtain the controller
             * that will process the request. If no controller is available,
             * return false and let other filters/servlets process the request.
             */
            Controller controller = controllerConfigurer.resolveControllerForRequest(request);
            if (controller == null) {
                logger.info("No controller configured for {}" ,request.getRequestURI());
                return false;
            }

            logger.info("Processing request {} with {}", request.getRequestURI(), controller.getClass());

            /*
             * Obtain the KTeeTemplateEngine instance.
             */
            ITemplateEngine templateEngine = controllerConfigurer.getTemplateEngine();

            /*
             * Write the response headers
             */
            response.setContentType("text/html;charset=UTF-8");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            logger.info("Filter {} passing control to {}", getClass().getSimpleName(),
                    controller.getClass().getSimpleName());
            /*
             * Execute the controller and process view template,
             * writing the results to the response writer. 
             */
            controller.process(request, response, this.servletContext, templateEngine);

            return true;

        } catch (Exception e) {
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (final IOException ignored) {
                // Just ignore this
            }
            throw new ServletException(e);
        }

    }


}
