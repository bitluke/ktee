package com.bfs.web.listener;

import org.hibernate.validator.HibernateValidator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidationContextListener implements ServletContextListener {

    public static final String VALIDATOR = "validator";
    private Validator validator;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ValidatorFactory validatorFactory =
                Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory();
        validator = validatorFactory.getValidator();
        sce.getServletContext().setAttribute(VALIDATOR, validator);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        validator = null;
    }
}
