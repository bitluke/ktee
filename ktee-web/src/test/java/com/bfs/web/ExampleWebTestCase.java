package com.bfs.web;

import org.junit.Before;
import org.junit.Test;

import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class ExampleWebTestCase {

    @Before
    public void prepare() {
        setBaseUrl("http://localhost:8080/ktee");
    }

    @Test
    public void testHomePage() {
        beginAt(""); //Open the browser on http://localhost:8080/test/home.xhtml
        assertTitleEquals("Karee Tee | Home");
        assertTitleEquals("Karee Tee | Home");
    }
}