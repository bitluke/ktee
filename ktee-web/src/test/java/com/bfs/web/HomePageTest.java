package com.bfs.web;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HomePageTest {

    @Test
    void testHomePage() {
        HomePage homePage = new HomePage(new HtmlUnitDriver(true));
        assertEquals("Karee Tee | Home", homePage.getTitle());
    }
}
