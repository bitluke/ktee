package com.bfs.web;

import org.openqa.selenium.WebDriver;

public class HomePage {

    private final WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.driver.get("http://localhost:8080/ktee/");
        if (!"Karee Tee | Home".equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the login page");
        }
    }

    public String getTitle(){
        return driver.getTitle();
    }
}
