package com.bfs.usecase.mapper;

import com.bfs.core.KTeeRole;
import com.bfs.usecase.kteerole.KTeeRoleDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface KteeRoleMapper {

    KteeRoleMapper INSTANCE = Mappers.getMapper(KteeRoleMapper.class);

    KTeeRole kTeeRoleDtoToKTeeRole(KTeeRoleDto kTeeRoleDto);

    KTeeRoleDto kTeeRoleToKTeeRoleDto(KTeeRole kTeeRole);

    List<KTeeRoleDto> kTeeRolesToKTeeRoleDtos(List<KTeeRole> kTeeRoles);

}
