package com.bfs.usecase.mapper;

import com.bfs.core.KTeePermission;
import com.bfs.usecase.kteepermission.KTeePermissionDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface KteePermissionMapper {

    KteePermissionMapper INSTANCE = Mappers.getMapper(KteePermissionMapper.class);

    KTeePermission KTeePermissionDtoTokTeePermission(KTeePermissionDto kTeePermissionDto);

    KTeePermissionDto kTeePermissionToKTeePermissionDto(KTeePermission kTeePermission);

    List<KTeePermissionDto> kTeePermissionsToKTeePermissionDtos(List<KTeePermission> kTeePermissions);

}
