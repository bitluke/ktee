package com.bfs.usecase.mapper;

import com.bfs.core.KTeeUser;
import com.bfs.usecase.kteeuser.KTeeUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface KteeUserMapper {

    KteeUserMapper INSTANCE = Mappers.getMapper(KteeUserMapper.class);

    KTeeUser kTeeUserDtoToKTeeUser(KTeeUserDto kTeeUserDto);

    KTeeUserDto kTeeUserToKTeeUserDto(KTeeUser kTeeUser);

    List<KTeeUserDto> kTeeUsersToKTeeUserDtos(List<KTeeUser> kTeeUsers);

}
