package com.bfs.usecase.kteerole;

import com.bfs.core.KTeeRole;
import com.bfs.repo.jdbc.PgDataSource;
import com.bfs.repo.kteerole.DefaultKteeRoleRepo;
import com.bfs.repo.kteerole.KteeRoleRepo;
import com.bfs.usecase.mapper.KteeRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public class DefaultKTeeRoleService implements KTeeRoleService {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KteeRoleRepo kteeRoleRepo;
    private KteeRoleMapper kteeRoleMapper = KteeRoleMapper.INSTANCE;

    public DefaultKTeeRoleService() {
        kteeRoleRepo = new DefaultKteeRoleRepo();
    }

    @Override
    public void createKteeRole(KTeeRoleDto kTeeRoleDto) {
        Connection connection = PgDataSource.getConnection();
        KTeeRole teeRole = kteeRoleMapper.kTeeRoleDtoToKTeeRole(kTeeRoleDto);
        kteeRoleRepo.create(teeRole, connection);
        PgDataSource.closeConnection(connection);;
    }

    @Override
    public KTeeRoleDto findKteeRole(UUID id) {
        Connection connection = PgDataSource.getConnection();
        KTeeRole teeRole = kteeRoleRepo.find(id, connection);
        if (teeRole != null) {
            return kteeRoleMapper.kTeeRoleToKTeeRoleDto(teeRole);
        }
        PgDataSource.closeConnection(connection);;
        return null;
    }

    @Override
    public void updateKteeRole(KTeeRoleDto kTeeRoleDto) {
        Connection connection = PgDataSource.getConnection();
        KTeeRole teeRole = kteeRoleMapper.kTeeRoleDtoToKTeeRole(kTeeRoleDto);
        kteeRoleRepo.update(teeRole, connection);
        PgDataSource.closeConnection(connection);;
    }

    @Override
    public List<KTeeRoleDto> listKTeeRoles() {
        Connection connection = PgDataSource.getConnection();
        List<KTeeRole> kTeeRoles = kteeRoleRepo.list(connection);
        PgDataSource.closeConnection(connection);;
        return kteeRoleMapper.kTeeRolesToKTeeRoleDtos(kTeeRoles);
    }

    @Override
    public void deleteKTeeRoles(UUID id) {
        Connection connection = PgDataSource.getConnection();
        kteeRoleRepo.delete(id, connection);
        PgDataSource.closeConnection(connection);
    }


}
