package com.bfs.usecase.kteerole;

import java.util.List;
import java.util.UUID;

public interface KTeeRoleService {

    public void createKteeRole(KTeeRoleDto kTeeRoleDto);

    public KTeeRoleDto findKteeRole(UUID id);

    public void updateKteeRole(KTeeRoleDto kTeeRoleDto);

    public List<KTeeRoleDto> listKTeeRoles();

    public void deleteKTeeRoles(UUID id);

}
