package com.bfs.usecase.kteeuser;

import java.util.List;
import java.util.UUID;

public interface KTeeUserService {

    public void createKteeUser(KTeeUserDto kTeeUserDto);

    public KTeeUserDto findKteeUser(UUID id);

    public void updateKteeUser(KTeeUserDto kTeeUserDto);

    public List<KTeeUserDto> listKTeeUsers();

    public void deleteKTeeUsers(UUID id);

}
