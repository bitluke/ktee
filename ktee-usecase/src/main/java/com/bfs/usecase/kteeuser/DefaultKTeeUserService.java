package com.bfs.usecase.kteeuser;

import com.bfs.core.KTeeUser;
import com.bfs.repo.jdbc.PgDataSource;
import com.bfs.repo.kteeuser.DefaultKteeUserRepo;
import com.bfs.repo.kteeuser.KteeUserRepo;
import com.bfs.usecase.mapper.KteeUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public class DefaultKTeeUserService implements KTeeUserService {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KteeUserRepo kteeUserRepo;
    private KteeUserMapper kteeUserMapper = KteeUserMapper.INSTANCE;

    public DefaultKTeeUserService() {
        kteeUserRepo = new DefaultKteeUserRepo();
    }

    @Override
    public void createKteeUser(KTeeUserDto kTeeUserDto) {
        Connection connection = PgDataSource.getConnection();
        KTeeUser teeUser = kteeUserMapper.kTeeUserDtoToKTeeUser(kTeeUserDto);
        kteeUserRepo.create(teeUser, connection);
        PgDataSource.closeConnection(connection);;
    }

    @Override
    public KTeeUserDto findKteeUser(UUID id) {
        Connection connection = PgDataSource.getConnection();
        KTeeUser teeUser = kteeUserRepo.find(id, connection);
        if (teeUser != null) {
            return kteeUserMapper.kTeeUserToKTeeUserDto(teeUser);
        }
        PgDataSource.closeConnection(connection);;
        return null;
    }

    @Override
    public void updateKteeUser(KTeeUserDto kTeeUserDto) {
        Connection connection = PgDataSource.getConnection();
        KTeeUser teeUser = kteeUserMapper.kTeeUserDtoToKTeeUser(kTeeUserDto);
        kteeUserRepo.update(teeUser, connection);
        PgDataSource.closeConnection(connection);;
    }

    @Override
    public List<KTeeUserDto> listKTeeUsers() {
        Connection connection = PgDataSource.getConnection();
        List<KTeeUser> kTeeUsers = kteeUserRepo.list(connection);
        PgDataSource.closeConnection(connection);;
        return kteeUserMapper.kTeeUsersToKTeeUserDtos(kTeeUsers);
    }

    @Override
    public void deleteKTeeUsers(UUID id) {
        Connection connection = PgDataSource.getConnection();
        kteeUserRepo.delete(id, connection);
        PgDataSource.closeConnection(connection);;
    }


}
