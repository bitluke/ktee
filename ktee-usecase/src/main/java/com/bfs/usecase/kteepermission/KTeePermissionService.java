package com.bfs.usecase.kteepermission;


import java.util.List;
import java.util.UUID;

public interface KTeePermissionService {

    public void createkTeePermission(KTeePermissionDto kTeePermissionDto);

    public KTeePermissionDto findkTeePermission(UUID id);

    public void updatekTeePermission(KTeePermissionDto kTeePermissionDto);

    public List<KTeePermissionDto> listkTeePermission();

    public void deletekTeePermission(UUID id);

}
