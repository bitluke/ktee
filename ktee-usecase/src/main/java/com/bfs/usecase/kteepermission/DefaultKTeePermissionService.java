package com.bfs.usecase.kteepermission;

import com.bfs.core.KTeePermission;
import com.bfs.repo.jdbc.PgDataSource;
import com.bfs.repo.kteepermission.DefaultKTeePermissionRepo;
import com.bfs.repo.kteepermission.KTeePermissionRepo;
import com.bfs.usecase.mapper.KteePermissionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.util.List;
import java.util.UUID;

public class DefaultKTeePermissionService implements KTeePermissionService {
    private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private KTeePermissionRepo kTeePermissionRepo;
    private KteePermissionMapper kteePermissionMapper = KteePermissionMapper.INSTANCE;

    public DefaultKTeePermissionService() {
        kTeePermissionRepo = new DefaultKTeePermissionRepo();
    }

    @Override
    public void createkTeePermission(KTeePermissionDto kTeePermissionDto) {
        Connection connection = PgDataSource.getConnection();

        KTeePermission permission =
                kteePermissionMapper.KTeePermissionDtoTokTeePermission(kTeePermissionDto);

        kTeePermissionRepo.create(permission, connection);


        PgDataSource.closeConnection(connection);
    }


    @Override
    public KTeePermissionDto findkTeePermission(UUID id) {
        Connection connection = PgDataSource.getConnection();
        KTeePermission permission = kTeePermissionRepo.find(id, connection);
        if (permission != null) {
            return kteePermissionMapper.kTeePermissionToKTeePermissionDto(permission);
        }
        PgDataSource.closeConnection(connection);
        return null;
    }

    @Override
    public void updatekTeePermission(KTeePermissionDto kTeePermissionDto) {
        Connection connection = PgDataSource.getConnection();
        KTeePermission permission = kteePermissionMapper.KTeePermissionDtoTokTeePermission(kTeePermissionDto);
        kTeePermissionRepo.update(permission, connection);
        PgDataSource.closeConnection(connection);
    }

    @Override
    public List<KTeePermissionDto> listkTeePermission() {
        Connection connection = PgDataSource.getConnection();
        List<KTeePermission> kTeePermissions = kTeePermissionRepo.list(connection);
        PgDataSource.closeConnection(connection);
        return kteePermissionMapper.kTeePermissionsToKTeePermissionDtos(kTeePermissions);
    }

    @Override
    public void deletekTeePermission(UUID id) {
        Connection connection = PgDataSource.getConnection();
        kTeePermissionRepo.delete(id, connection);
        PgDataSource.closeConnection(connection);
    }


}
